	list	p=16f877a			;Specifies which micro is used 
	#include	<p16f877a.inc>	;Header file for PIC flie
	__CONFIG	_CP_OFF & _WDT_OFF & _BODEN_OFF & _PWRTE_ON & _HS_OSC & _WRT_OFF & _LVP_OFF & _CPD_OFF		;Various config options 

;=====================================================================================================
; Definitions
;=====================================================================================================
#define	LCD_RS			PORTE,0		;Define the pin for the LCD RS Control Line
#define	LCD_RW			PORTE,1		;Define the pin for the LCD R/W Control Line
#define	LCD_E	    	PORTE,2		;Define the pin for the LCD E Control Line
#define	LCD_DATA		PORTD	    ;Define the pins for LCD Data Lines
#define UNLOCK_PIN		PORTB, RB0	;Pin that receieves unlock signal
#define STEP_MODE_BTN	PORTB, RB2	;Step mode button
#define	POS_MODE_BTN	PORTB, RB3	;Position Mode button
#define INC_STEP_BTN	PORTB, RB4	;Increment Steps button
#define	DEC_STEP_BTN	PORTB, RB5	;Decrement steps button
#define CENTRE_BTN	PORTB, RB6	;Centre Button
#define DE_ENERGISE b'00111001' ;PORTC Bit Definition to de energise all motor windings
#define	A1			b'111100'	;PORTC Bit Definition to energise winding A1 (#define defines constants)
#define	A1_B1		b'101110'	;PORTC Bit Definition to hstep
#define	A2			b'110101'	;PORTC Bit Definition to energise winding A2
#define	A2_B2		b'010111'	;PORTC Bit Definition to hstep
#define	B1			b'101011'	;PORTC Bit Definition to energise winding B1
#define	B1_A2		b'100111'	;PORTC Bit Definition to hstep
#define	B2			b'011011'	;PORTC Bit Definition to energise winding B2
#define	B2_A1		b'011110'	;PORTC Bit Definition to hstep

;=====================================================================================================
; Variables
;=====================================================================================================
COUNTER1		EQU	0x020			;Memory address for Counter 1
COUNTER2		EQU	0x021			;Memory address for Counter 2
COUNTER1		EQU	0x020			;Counter1 at memory address 0x020
COUNTER2		EQU	0x021			;Counter2 at memory address 0x021
COUNTER3		EQU	0x022			;COUNTER3 at memory address 0x022
stepperDirection EQU 0x023			;Register that contains stepper direction bit 
temp			EQU 0x024			;Temporary variable at 0x024
digitCount		EQU 0x025			;digitCount variable at 0x024
firstDigit 		EQU 0x026
secondDigit 	EQU 0x027
thirdDigit		EQU 0x028
stepCounter		EQU 0x029
ADCValue		EQU	0x030
previousADCValue	EQU 0x031
singleStepCounter	EQU 0x032
;======================================================================================================
; ********************** PROGRAM START *******************************
;======================================================================================================

	ORG	0x0000	
RESET_V	GOTO	START			;Goto start on reset

	ORG	0x0005
START	
	clrf stepCounter
	call IO_SETUP				;Setup IO
	call ADC_SETUP				;Setup ADC
	call LCD_SETUP				;Setup LCD
	call WRITE_LOCKED_MODE		;Write locked mode & student number to LCD
	call CHECK_INPUT_PIN		;Start looking for signal from CPLD
	call WRITE_IDLE_MODE		;If pin goes high, write "Idle mode, signal received" to LCD
	goto GET_USER_INPUT			;Get a button press from the user (Program mode, step mode etc.)			
	;Code does not reach this line
HANG							;Code hangs here indefinitely
	goto HANG

IO_SETUP
	clrf	PORTA				;Clear PORT A Output latches
	bsf		STATUS,RP0			;Switch to bank 1 
	movlw	0x07		
	movwf	ADCON1				;Make RA0-RA3 Digital I/O 		
	bcf		TRISB, RB0			;RA0 output
	bsf		TRISB, RB1			;RA1 input - This is the unlock pin
	bsf		TRISB, RB2			;RA2 input - This is the step mode pin
	bsf		TRISB, RB3			;RB3 input - Pos mode button
	bsf		TRISB, RB4			;RB4 input - increment steps button	
	bsf		TRISB, RB5 			;RB5 input - decrement steps button
	bsf		TRISB, RB6			;RB6 input - Centre/enter button
	;Set up stepper
	movlw	B'00000000'			;Load an 8 bit value into the working register to make pins portc outputs
	movwf	TRISC				;Load the working register into TRISC
	bcf 	STATUS, RP0			;Switch to bank 0
	bsf		stepperDirection, 1	;Set the 'zero' bit of stepperDirection i.e. stepper is currently at position 0 
	return
ADC_SETUP
	clrf	PORTA		;Clear PORTB Output Latches.
	bsf	STATUS,RP0		;Switch to Bank 1.
	movlw	0xFF		;Switch PortA to all inputs
	movwf	TRISA		
    movlw	b'00000000' ;Result left justified. All portA pins analog
	movwf	ADCON1		;Set all pins to analogue input.Result left justified
	bcf	STATUS,RP0		;Switch to Bank 0
	movlw	b'10100001'	;Set ADC to Fosc/32, channel 4, adc on
	movwf	ADCON0	
	return	
LCD_SETUP
	clrf PORTD		      		;Clear PORTD Output Latches.
	clrf PORTE		      		;Clear PORTE Output Latches.
	bsf	STATUS,RP0 				;Switch to Bank 1.
	movlw 0X07 					;Load W with 0x07
	movwf ADCON1				;Copy W into ADCON1 (Switch Port A and E into Digital Mode)
    movlw 0x00
	movwf TRISD					;Switch all Port D pins to output mode
    movlw 0x00
	movwf TRISE					;Switch all Port E pins to output mode
	bcf	STATUS,RP0				;Switch to Bank 0
	bcf LCD_RW             		;Clear LCD_RW line to write data to the LCD
	bcf LCD_RS  	     		;Clear LCD_RS line to write control commands
	movlw	B'00111000'      	;Load the function set command into working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	movlw	B'00001110'      	;Load the Display On command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	movlw	B'00000110'      	;Load the Entry Mode Set command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	movlw	B'00000001'	     	;Load the Clear Screen command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	call LONG_DELAY_LCD	     	;Call the Long_Delay Subroutine
	bsf  LCD_RS		     		;Set the LCD_RS control line to write characters to the display
	return
CLEAR_LCD
	bcf LCD_RW             		;Clear LCD_RW line to write data to the LCD
	bcf LCD_RS  	     		;Clear LCD_RS line to write control commands
	movlw	B'00000001'	     	;Load the Clear Screen command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	call LONG_DELAY_LCD	     	;Call the Long_Delay Subroutine
	bsf  LCD_RS		     		;Set the LCD_RS control line to write characters to the display
	return
;=========================================================================================================
; *******************************************DISPLAYS****************************************************
;=========================================================================================================
WRITE_LOCKED_MODE
	movlw 'L'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
    movlw 'o'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'c'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'k'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'e'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'd'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw ' '		    		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'M'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'o'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'd'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'e'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	call NEW_LINE				;Write new line to LCD
	bsf LCD_RS		    		;Set the LCD_RS control line to write characters to the display		
	movlw '1'		      		;"S"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
    movlw '1'		      		;"E"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '9'		      		;"C"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '9'		      		;"O"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '7'		      		;"N"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '0'		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '7'		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw '1'		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	return						;Return to where this function was called from 

;=========================================================================================================
; *****************************		Check Input Pin - Security		**************************************
;=========================================================================================================
CHECK_INPUT_PIN	
	btfsc	UNLOCK_PIN			;Wait for low signal
	goto	CHECK_INPUT_PIN
WAIT_FOR_RISING_EDGE	
	btfss	UNLOCK_PIN
	goto 	WAIT_FOR_RISING_EDGE	;Han
	call	LONG_DELAY_LCD		;delay for 6ms (roughly 1 half period)
	btfsc	UNLOCK_PIN			;Check pin again 
	goto	CHECK_INPUT_PIN		;The signal did not match the 100Hz input
	call	LONG_DELAY_LCD		;delay for 6ms (roughly 1 half period)
	btfss	UNLOCK_PIN
	goto 	CHECK_INPUT_PIN
	call	LONG_DELAY_LCD
	btfsc	UNLOCK_PIN
	goto	CHECK_INPUT_PIN	
	return
WRITE_IDLE_MODE
	bcf LCD_RW             		;Clear LCD_RW line to write data to the LCD
	bcf LCD_RS  	     		;Clear LCD_RS line to write control commands
	movlw	B'00000001'	     	;Load the Clear Screen command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	call LONG_DELAY_LCD	     	;Call the Long_Delay Subroutine
	bsf  LCD_RS		     		;Set the LCD_RS control line to write characters to the display
	movlw 'I'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
    movlw 'd'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'l'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'e'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw ' '		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'M'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'o'		    		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'd'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'e'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw ' '		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	call NEW_LINE				;Write a new line to the LCD
	movlw 'S'		      		;"S"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
    movlw 'i'		      		;"E"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'g'		      		;"C"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'n'		      		;"O"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'a'		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'l'		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw ' '		      		;"D"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'R'		      		;"S"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
    movlw 'e'		      		;"E"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'c'		      		;"C"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'e'		      		;"O"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'i'		      		;"S"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
    movlw 'v'		      		;"E"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'e'		      		;"C"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	movlw 'd'		      		;"O"Load the first letter of your Surname into the Working Register
	call LCD_WRITE          	;Call the LCD_Write Subroutine
	return						;Return to where this function was called from 	
WRITE_STEP_MODE
	bcf LCD_RW             		;Clear LCD_RW line to write data to the LCD
	bcf LCD_RS  	     		;Clear LCD_RS line to write control commands
	movlw	B'00000001'	     	;Load the Clear Screen command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	call LONG_DELAY_LCD	     	;Call the Long_Delay Subroutine
	bsf  LCD_RS		     		;Set the LCD_RS control line to write characters to the display
	movlw 'S'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
    movlw 't'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'e'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'p'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw ' '		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'M'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'o'		    		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'd'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'e'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	call NEW_LINE				;Write new line to the LCD
	movlw 'S'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine 
  	movlw 'T'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'E'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'P'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'S'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw ':'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	return						;Return to where this function was called from
WRITE_POS_MODE
	bcf LCD_RW             		;Clear LCD_RW line to write data to the LCD
	bcf LCD_RS  	     		;Clear LCD_RS line to write control commands
	movlw	B'00000001'	     	;Load the Clear Screen command into the working register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	call LONG_DELAY_LCD	     	;Call the Long_Delay Subroutine
	bsf  LCD_RS		     		;Set the LCD_RS control line to write characters to the display
	movlw 'P'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
    movlw 'o'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 's'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'i'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 't'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'i'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'o'		    		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw 'n'		     		; "T" Write the remaining letters of your First Name to the screen
	call LCD_WRITE	
	movlw ' '		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	movlw 'M'		     		; "F" Load the first letter (ASCII Character value) of your First Name into the Working Register
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
    movlw 'o'		     		; "I" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'd'		     		; "R" Write the remaining letters of your First Name to the screen
	call LCD_WRITE
    movlw 'e'		     		; "S" Write the remaining letters of your First Name to the screen
	call LCD_WRITE

	call NEW_LINE				;Write new line to the LCD
	return
;=========================================================================================================
; ********************************* 	Main Routine	**************************************************
;=========================================================================================================
GET_USER_INPUT
	BTFSS STEP_MODE_BTN			;Check if step mode button has been pressed (active high)
	call DEBOUNCE_STEP_MODE_BTN	;Debounce button press
	BTFSS POS_MODE_BTN			;Check if position mode button has been pressed (active high)
	call DEBOUNCE_POS_MODE_BTN	;Debounce button press & run mode
	goto GET_USER_INPUT			;Repeat this loop indefinitely
MAIN_MENU
	btfss 	POS_MODE_BTN		;Wait for current button to be released
	goto 	MAIN_MENU
	btfss	STEP_MODE_BTN
	goto	MAIN_MENU			;Wait for current button to be released
	call 	WRITE_IDLE_MODE
	goto 	GET_USER_INPUT
;=========================================================================================================
; ********************************* Debounce Mode Buttons	**************************************************
;=========================================================================================================

DEBOUNCE_STEP_MODE_BTN
	call LONG_DELAY_LCD			;delay for 6ms
	BTFSC STEP_MODE_BTN			;Check pin again
	return						;If pin is no longer set, return
	call LONG_DELAY_LCD			;delay for another 6ms (total 12ms)
	BTFSC STEP_MODE_BTN			;Check pin again
	return						;If pin is no longer set, return
	call LONG_DELAY_LCD			;delay for another 6ms (total 18ms)
	BTFSC STEP_MODE_BTN			;Check pin one more time
	return						;If pin is no longer set, return
	call STEP_MODE				;This press has been successully debounced. Continue to Step Mode
	return						;Once step mode has completed, return to main get user input loop

DEBOUNCE_POS_MODE_BTN
	call LONG_DELAY_LCD			;delay for 6ms
	BTFSC POS_MODE_BTN			;Check pin again
	return						;If pin is no longer set, return
	call LONG_DELAY_LCD			;delay for another 6ms (total 12ms)
	BTFSC POS_MODE_BTN			;Check pin again
	return						;If pin is no longer set, return
	call LONG_DELAY_LCD			;delay for another 6ms (total 18ms)
	BTFSC POS_MODE_BTN			;Check pin one more time
	return						;If pin is no longer set, return
	call POS_MODE				;This press has been successully debounced. Continue to Step Mode
	return						;Once step mode has completed, return to main get user input loop
;=========================================================================================================
; ********************************* 	Step  Mode	**************************************************
;=========================================================================================================	
STEP_MODE
	call WRITE_STEP_MODE
	goto GET_STEP_INPUT
	return
GET_STEP_INPUT
	btfss 	INC_STEP_BTN			;Check if increment step mode button has been pressed (active high)
	call 	DEBOUNCE_INCREMENT_BTN	;Debounce button press
	btfss	DEC_STEP_BTN				;Check if decrement step mode button has been pressed
	call 	DEBOUNCE_DECREMENT_PIN	;Debounce button press
	btfss	CENTRE_BTN				;Check if center/enter button has been pressed
	call 	DEBOUNCE_CENTER_PIN		;Debounce center pin
	btfss	POS_MODE_BTN			;	
	goto 	MAIN_MENU
	goto 	GET_STEP_INPUT			
	
DEBOUNCE_INCREMENT_BTN 
	call 	LONG_DELAY_LCD		;delay for 6ms
	BTFSC 	PORTB, RB4			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 12ms)
	BTFSC 	PORTB, RB4			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 18ms)
	BTFSC 	PORTB, RB4			;check pin one more time
	return						;If pin is no longer set, return
WAIT_UNTIL_RELEASE1	btfss	PORTB, RB4
	goto 	WAIT_UNTIL_RELEASE1	
	btfss	stepperDirection,1	;Check if motor is at position 0
	btfss 	stepperDirection,0	;If motor is CW, increment, otherwise decrement
	goto	INCREMENT			;Value is CCW, Increment value
	goto	DECREMENT			;Value is CW decrement value
	return
INCREMENT
	incf	stepCounter,f		;Increment CW step counter
	call 	WRITE_STEP_MODE 
	movf 	stepCounter, w		;Move stepCounter to working register
	call 	CONVERT_WRITE
	btfss 	stepperDirection, 0 ;Check for the stepper direction flag
	call 	WRITE_CW
	btfsc 	stepperDirection, 0 ;Check for the stepper direction flag
	call	WRITE_CCW
	bcf 	stepperDirection,1	;Clear 'zero' bit - motor is not at position 0
	return						;Return to where this function was called from 
CLOCKWISE
	bsf stepperDirection, 0		;Change to CCW
	return
DECREMENT
	btfsc	stepperDirection, 1		;Change direction if we're at position 0
	goto 	CLOCKWISE				
	decfsz	stepCounter,f			;Decrement CW step counter
	goto	DECREMENT_STEP_COUNTER
	goto	CHANGE_DIRECTION
	return
CHANGE_DIRECTION
	movlw 	0x01					;Move 1 into working regsiter
	xorwf 	stepperDirection		;Set bit if 0, clear bit if 1
	movf 	stepCounter, w			;Move stepCounter to working register
	call 	WRITE_STEP_MODE 
	movf 	stepCounter, w			;Move stepCounter to working register
	call 	CONVERT_WRITE
	btfss 	stepperDirection, 0 ;Check for the stepper direction flag
	call 	WRITE_CW
	btfsc 	stepperDirection, 0 ;Check for the stepper direction flag
	call	WRITE_CCW
	bsf		stepperDirection, 1		;Set the 'zero' bit of stepper. Explained in initialisation
	return
DECREMENT_STEP_COUNTER	
	call 	WRITE_STEP_MODE 
	movf 	stepCounter, w	;Move stepCounter to working register;
	call 	CONVERT_WRITE
	btfss 	stepperDirection, 0 ;Check for the stepper direction flag
	call 	WRITE_CW
	btfsc 	stepperDirection, 0 ;Check for the stepper direction flag
	call	WRITE_CCW
	return						;Return to where this function was called from	
DEBOUNCE_DECREMENT_PIN
	call 	LONG_DELAY_LCD		;delay for 6ms
	btfsc 	PORTB, RB5			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 12ms)
	BTFSC 	PORTB, RB5			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 18ms)
	BTFSC 	PORTB, RB5			;check pin one more time
	return						;If pin is no longer set, return
WAIT_UNTIL_RELEASE2	btfss	PORTB, RB5
	goto 	WAIT_UNTIL_RELEASE2
	btfss 	stepperDirection,0	
	goto	DECREMENT			;Value is CW decrement value
	goto	INCREMENT			;Value is CCW, Increment value
	return						;return to where this code was called from
CONVERT_WRITE
	call 	CONVERT_TO_BCD
	call 	WRITE_BCD_VALS
	return
DEBOUNCE_CENTER_PIN
	call 	LONG_DELAY_LCD		;delay for 6ms
	BTFSC 	PORTB, RB6			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 12ms)
	BTFSC 	PORTB, RB6			;Check pin again
	return						;If pin is no longer set, return
	call 	LONG_DELAY_LCD		;delay for another 6ms (total 18ms)
	btfsc 	PORTB, RB6			;check pin one more time
	return						;If pin is no longer set, return
WAIT_UNTIL_RELEASE3	btfss	PORTB, RB6
	goto 	WAIT_UNTIL_RELEASE3	;Wait for button to be released
DECREMENT_STEP_COUNT	
	decfsz 	stepCounter, f		;Decrement stepCounter until 0
	goto 	RUN_STEPPER
	goto 	FINISHED_STEPS		;Finish routine when stepCounter = 0
	return
RUN_STEPPER 
	btfss 	stepperDirection,0
	call	SINGLE_STEP_CW
	;call	ROTATE_LOOP_CW
	btfsc	stepperDirection, 0
	call 	SINGLE_STEP_CCW		;Call rotate loop
	call	WRITE_STEP_MODE		
	movf 	stepCounter, w		;Move stepCounter to working register
	call 	CONVERT_TO_BCD
	call 	WRITE_BCD_VALS
	btfss 	stepperDirection,0
	call	WRITE_CW
	btfsc	stepperDirection, 0
	call 	WRITE_CCW
	goto 	DECREMENT_STEP_COUNT
FINISHED_STEPS
	btfss 	stepperDirection,0	;Test if we should be turning clockwise or anticlockwise	
	call	SINGLE_STEP_CW	
;call	ROTATE_LOOP_CW	
	btfsc	stepperDirection, 0
	call 	SINGLE_STEP_CCW		;Call rotate loop
	call 	WRITE_STEP_MODE
	movf 	stepCounter, w		;Move stepCounter to working register
	call 	CONVERT_TO_BCD
	call 	WRITE_BCD_VALS
	bcf 	stepperDirection, 0	;Clear direction bit to default
	bsf		stepperDirection, 1	;Set 'zero' bit - Motor is at position 0
	return	

;=========================================================================================================
; ********************************* 	Position Mode	**************************************************
;=========================================================================================================
POS_MODE
	call WRITE_POS_MODE
posInput	call GET_POS_INPUT
	call SAMPLE_ADC
	call COMPARE_ADC
	call LONG_DELAY_LCD
	call LONG_DELAY_LCD
	call LONG_DELAY_LCD
	goto posInput				;Loop here
GET_POS_INPUT		;
	btfss	STEP_MODE_BTN
	goto 	MAIN_MENU
	return
SAMPLE_ADC
	bsf		ADCON0,2			;set GO_DONE bit to start conversion
	btfsc	ADCON0,2			;test GO_DONE bit to see if conversion complete
	goto	$-1		      		;keep checking GO_DONE bit until cleared
	movf	ADRESH,w			;move adc result into the working register
	movwf 	ADCValue			;Move result into ADCValue register
	return
COMPARE_ADC		
	movf 	ADCValue, w
	subwf	previousADCValue, w			;Check if this value is different from the previous value
	btfsc	STATUS,Z	 		;test to see if ADCValue is zero	
	return
	movwf	previousADCValue	;Move current ADC value to previousADCValue register - will be used next time
	call	CLEAR_LCD
	call 	WRITE_POS_MODE		;Write Position mode to screen
	movf	ADCValue, w			;Move current ADC value to working register
	call	CONVERT_WRITE		;Convert the values to BCD and write to LCD
	return

DELAY	movlw 	0x21			;Load hex 30 	
	movwf 	COUNTER1			;Copy W into Counter 1		
C2_LOAD	movlw	0x65			;Load 101 in W 
	movwf	COUNTER2			;Copy W into Counter 2 
C3_LOAD	movlw	0xFA			;Load 250 into W
	movwf	COUNTER3			;Copy W into Counter 3
C3_DEC	decfsz	COUNTER3,f		;Decement Counter 3
	goto 	C3_DEC				;If not 0, keep decrementing timer
C2_DEC	decfsz	COUNTER2,f		;Decrement Counter 2 
	goto	C3_LOAD				;If not 0, reinitialise counter 3 and decrement again 
C1_DEC	decfsz	COUNTER1,f		;Decrememnt Counter 1	
	goto	C2_LOAD				;If not 0, reinitialise counter 2 and decrement again
	return 						;Delay routine is complete, go back where the routine was called from
;********************************************************************************************************
; LCD Sub routine section
;********************************************************************************************************
;********************************************************************************************************
; LCD Write - loads a command into the LCD Controller
;********************************************************************************************************
LCD_WRITE		
	bsf 	LCD_E	 			;Set the LCD Enable Pin
	movwf LCD_DATA				;Write the command in the working register to the LCD Data Lines
	nop							;No Operation
	nop							;No Operation
	bcf 	LCD_E				;Clear the LCD Enable Pin
	call  SHORT_DELAY_LCD		;Call the Short_Delay subroutine to Wait the minimum instruction execution time
	return		      			;Return to where the routine was called from

;********************************************************************************************************
; Short Delay (approx 60us)
;********************************************************************************************************
SHORT_DELAY_LCD	
	 movlw 0x64 		;Load 100 into the Working register
	 movwf COUNTER1	      ;Copy the Working register into the Counter1 register
C1_DEC_LCD decfsz	COUNTER1,f 	;Decrement the Counter 1 register
	Goto C1_DEC_LCD		      ;Goto C1_DEC
	return		      ;Return to where the routine was called from
;********************************************************************************************************
; Long Delay (approx 6ms)
;********************************************************************************************************
LONG_DELAY_LCD movlw 0x64 		;Load 100 into the Working register
	     movwf COUNTER2     	;Copy the Working register into the Counter2 register
DLY_C2_DEC_LCD call SHORT_DELAY_LCD	;Call the SHORT_DELAY subroutine
	     decfsz	COUNTER2,f		;Decrement the Counter 2 register
	     goto	DLY_C2_DEC_LCD	;Goto DLY_C2_DEC
	     return		      		;Return to where the routine was called from
NEW_LINE
	bcf LCD_RS		     		;Clear LCD_RS line to write control commands
	movlw	B'11000000'	     	;Load the command into the working register that places the cursor on the second line of the LCD
	call LCD_WRITE	     		;Call the LCD_Write Subroutine
	bsf LCD_RS		    		;Set the LCD_RS control line to write characters to the display
	return						;Return to where this function was called from 
;********************************************************************************************************
; BCD Convertion Subroutines
;********************************************************************************************************
CONVERT_TO_BCD
	call 	GET_HNDS			;Get the number of hundreds in this sequence 
	movwf 	firstDigit			;Copy current value into first digit register	
		
	movlw 	D'100'			
	addwf	temp				;Add 100 to temp
	movf 	temp, w				;move temp to working register
	call	GET_TENS
	movwf	secondDigit			;Move result to secondDigit variable

	movlw 	D'10'					
	addwf	temp				;Add 100 to temp
	movf 	temp, w				;move temp to working register
	call	GET_ONES
	movwf	thirdDigit			;Move result to secondDigit variable
	return
WRITE_BCD_VALS	
	movlw 	D'48'				;Copy 48(DEC) into working register 
	addwf 	firstDigit,f		;Add decimal 48 to result to convert to ASCII
	movf 	firstDigit, w		;Move the first digit into the working register
	call 	LCD_WRITE			;Write this number to the LCD	

	movlw 	D'48'				;Copy 48(DEC) into working register 
	addwf 	secondDigit,f		;Add decimal 48 to result to convert to ASCII
	movf 	secondDigit, w		;Move the first digit into the working register
	call 	LCD_WRITE			;Write this number to the LCD	
	
	movlw 	D'48'				;Copy 48(DEC) into working register 
	addwf 	thirdDigit,f		;Add decimal 48 to result to convert to ASCII
	movf 	thirdDigit, w		;Move the first digit into the working register
	call 	LCD_WRITE			;Write this number to the LCD		

	movlw 	' '					
	call 	LCD_WRITE			;Write this number to the LCD	
	return

WRITE_CW
	movlw 	'C'					;Copy 48(DEC) into working register 
	call 	LCD_WRITE			;Write this number to the LCD
	movlw 	'W'					;Copy 48(DEC) into working register 
	call 	LCD_WRITE			;Write this number to the LCD
 	return
WRITE_CCW
	movlw 	'C'					;Copy 48(DEC) into working register 
	call 	LCD_WRITE			;Write this number to the LCD
	movlw 	'C'					;Copy 48(DEC) into working register 
	call 	LCD_WRITE			;Write this number to the LCD
	movlw 	'W'					;Copy 48(DEC) into working register 
	call 	LCD_WRITE			;Write this number to the LCD
 	return
	
GET_HNDS	
	movwf 	temp				;Copy the working register value to t1
	clrf 	digitCount			;Clear the digit cont
GET_HNDS_LOOP
	movlw 	D'100'				;Copy 100(DEC) into working register
	incf 	digitCount, f		;Increment digitCounter and write value to digitCounter
	subwf 	temp, f				;Subtract 100 from temp register
	btfsc 	STATUS, C			;Check status register for carry bit
	goto	GET_HNDS_LOOP		;Repeat loop 	
	decf	digitCount, w 		;Decrement digitCount
	return

GET_TENS
	movwf 	temp				;Move the working register to the temp variable
	clrf 	digitCount			;Clear the digitCount register
GET_TENS_LOOP
	movlw 	D'10'				;Move 10(DEC) into the working register
	incf	digitCount, f		;Increment the digit count
	subwf	temp, f				;Subtract 10 from temp & store the variable in temp
	btfsc	STATUS, C			;Check status register for carry bit
	goto	GET_TENS_LOOP
	decf	digitCount, w
	goto	GET_ONES
GET_ONES
	movwf 	digitCount
	movlw	D'10'
DEL_TENS_LOOP
	subwf 	digitCount, f
	btfsc	STATUS, C			;Check status register for carry bit
	goto	DEL_TENS_LOOP
	addwf	digitCount, w
	return

;********************************************************************************************************
; Stepper SubRoutines
;********************************************************************************************************
;********************************************************************************************************
; Delay (approx 10ms)
;********************************************************************************************************
DELAY10ms	movlw D'60'		;Load decimal 66 into the working register
			movwf COUNTER2	;Move the working register into the COUNTER2 register
DLY_C2		call DELAY150us		;Call the DELAY150us routine
			decfsz COUNTER2,f	;Decrement COUNTER2 register and check if the result is Zero
			goto DLY_C2			;Counter 2 is NOT Zero: goto DLY_C2
			return				;Counter 2 is Zero: Return to where the routine was called from
;********************************************************************************************************
; Step Delay
;********************************************************************************************************
STEP_DELAY	movlw D'10'			;Load decimal 10 into the working register
			movwf COUNTER1		;Move the working register into the COUNTER1 register

DLY_C1 		call DELAY10ms		;Call the DELAY10ms routine
			decfsz COUNTER1,f	;Decrement COUNTER1 register and check if the result is Zero
			goto DLY_C1			;Counter 1 is NOT Zero: goto DLY_C1
			return				;Counter 1 is Zero: Return to where the routine was called from

;********************************************************************************************************
; Delay 150us
;********************************************************************************************************
DELAY150us	movlw D'250'		;Load decimal 250 into the working register
			movwf COUNTER3		;Move the working register into the COUNTER3 register

DLY_C3		decfsz COUNTER3,f	;Decrement COUNTER3 register and check if the result is Zero
			goto DLY_C3			;Counter 3 is NOT Zero: goto DLY_C3
			return				;Counter 3 is Zero: Return to where the routine was called from
;********************************************************************************************************
; Single Step (Clockwise)
;********************************************************************************************************
SINGLE_STEP_CW
	btfss	singleStepCounter,0 		;Check if the single step has been made 
	goto 	FIRST_CW_STEP
	btfss	singleStepCounter,1
	goto	SECOND_CW_STEP
	btfss	singleStepCounter,2 
	goto	THIRD_CW_STEP
	goto 	FOURTH_CW_STEP
	return
FIRST_CW_STEP
	bsf 	singleStepCounter, 0
	call 	STEP_A1
	return					;Exit from stepper commands	
SECOND_CW_STEP
	bsf 	singleStepCounter, 1
	call	STEP_B1
	return
THIRD_CW_STEP
	bsf		singleStepCounter, 2
	call	STEP_A2
	return
FOURTH_CW_STEP
	bcf 	singleStepCounter, 0	;Clear all step state flags
	bcf		singleStepCounter, 1
	bcf		singleStepCounter, 2
	call 	STEP_B2
	return
;********************************************************************************************************
; Single Step (Counter - Clockwise)
;********************************************************************************************************
SINGLE_STEP_CCW
	btfss	singleStepCounter,4 		;Check if the single step has been made 
	goto 	FIRST_CCW_STEP
	btfss	singleStepCounter,5
	goto	SECOND_CCW_STEP
	btfss	singleStepCounter,6 
	goto	THIRD_CCW_STEP
	goto 	FOURTH_CCW_STEP
	return
FIRST_CCW_STEP
	bsf 	singleStepCounter, 4
	call 	STEP_A1
	return					;Exit from stepper commands	
SECOND_CCW_STEP
	bsf 	singleStepCounter, 5
	call	STEP_B1
	return
THIRD_CCW_STEP
	bsf		singleStepCounter, 6
	call	STEP_A2
	return
FOURTH_CCW_STEP
	bcf 	singleStepCounter, 4	;Clear all step state flags
	bcf		singleStepCounter, 5
	bcf		singleStepCounter, 6
	call 	STEP_B2
	return

STEP_A1
	movlw	A1				;Load the 8 bit "STEP_1" definition into the working register
	movwf	PORTC	    	;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call	STEP_DELAY		;Call the Step Delay Routine	
	return
STEP_A2
	movlw 	A2			;Load the 8 bit "STEP_3" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call the 10ms Delay Routine
	return
STEP_B1
	movlw 	B1				;Load the 8 bit "STEP_2" definition into the working register
	movwf 	PORTC		    ;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call 	STEP_DELAY		;Call the Step Delay Routine
	return	
STEP_B2	
	movlw 	B2			;Load the 8 bit "STEP_4" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call step Delay Routine
	return
;********************************************************************************************************
; Stepper rotate loop (Clockwise)
;********************************************************************************************************
ROTATE_LOOP_CW

	movlw	A1				;Load the 8 bit "STEP_1" definition into the working register
	movwf	PORTC	    	;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call	STEP_DELAY		;Call the Step Delay Routine
   	
	movlw 	B1				;Load the 8 bit "STEP_2" definition into the working register
	movwf 	PORTC		    ;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call 	STEP_DELAY		;Call the Step Delay Routine

	movlw 	A2			;Load the 8 bit "STEP_3" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call the 10ms Delay Routine
	
	movlw 	B2			;Load the 8 bit "STEP_4" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call step Delay Routine	

    return					;Return to where this function was called from 
;********************************************************************************************************
; Stepper rotate loop (Counter-Clockwise)
;********************************************************************************************************
ROTATE_LOOP_CCW

	movlw	B1		;Load the 8 bit "STEP_1" definition into the working register
	movwf	PORTC	    ;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call	STEP_DELAY	;Call the Step Delay Routine
   	
	movlw 	A1		;Load the 8 bit "STEP_2" definition into the working register
	movwf 	PORTC		    ;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
 	call 	STEP_DELAY		;Call the Step Delay Routine

	movlw 	B2		;Load the 8 bit "STEP_3" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call the 10ms Delay Routine
	
	movlw 	A2		;Load the 8 bit "STEP_4" definition into the working register
	movwf 	PORTC			;Move the working register into the PORTC Register
	call	DELAY10ms		;Call the delay routine
	movlw	DE_ENERGISE		;Load DE_ENERGISE into working register
	call 	STEP_DELAY		;Call step Delay Routine	
	
    return				;Return to where this function was called from 

END	

;********************************************************************************************************
; References
;	
;	- Debouncing
;		http://forum.allaboutcircuits.com/threads/debounce-pushbutton-connected-to-uc.78937/
;********************************************************************************************************